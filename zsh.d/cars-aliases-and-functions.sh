# Aliases
alias src="cd ~/src"
alias car="cd ~/src/cars_platform"
alias split="make -C apps/experimentation start_splitd"

function s() {
  if [[ $PWD/ = */src/cars_platform/* ]]; then
    cd ~/src/cars_platform
    direnv allow
    mix deps.get
    iex -S mix phx.server
  elif [[ $PWD/ = */code/elixir/quick_average* ]]; then
    cd ~/code/elixir/quick_average
    iex -S mix phx.server
  elif [[ $PWD/ = */code/elixir/mediate* ]]; then
    cd ~/code/elixir/mediate
    iex -S mix phx.server
  else
    echo 'This build script currently only works for Cars Platform, Mediate and QuickAverage'
  fi
}

function b() {
  if [[ $PWD/ = */src/cars_platform/* ]]; then
    cd ~/src/cars_platform
    rm -rf _build
    say done
    mix deps.get
    mix compile
    say done
    mix phx.server
  elif [[ $PWD/ = */code/elixir/mediate/* ]]; then
    cd ~/code/elixir/mediate
    rm -rf _build
    say done
    mix deps.get
    mix compile
    say done
    mix phx.server
  else
    echo 'This build script currently only works for Cars Platform and Mediate'
  fi
}

function review() {
  cd ~/src/cars_platform

  # Check for any changes
  if [[ $(git status --porcelain) ]]; then
    say "Git status is not clean."
    echo "Git status is not clean. ❌"
    return 1
  fi

  echo "No unstaged or uncommitted changes. Proceeding with review. ✅"

  if [ -z "$1" ]; then
    say "Please provide a branch name."
    echo "Please provide a branch name. ❌"
    return 1
  fi

  git checkout main
  git pull

  git checkout "$1"
  if [ $? -ne 0 ]; then
    echo "Failed to checkout branch '$1'. ❌"
    return 1
  fi

  echo "Checked out branch '$1'. Proceeding with review. ✅"
  git pull
  asdf install
  direnv allow

  echo "Getting deps"
  mix deps.get
  echo "Finished getting deps ✅"
  say "Finished getting deps"

  mix ecto.migrate
  say "Finished migrating"

  # Try to start Phoenix server without removing _build
  echo "Trying to start Phoenix server without removing _build 🔄"
  if ! iex -S mix phx.server; then
    echo "Failed to start Phoenix server. Trying again after removing _build 🔄"
    say "Failed to start Phoenix server. Trying again after removing _build"

    echo "Going nuclear 💥"
    mix clean --all
    rm -rf _build

    echo "Getting deps 🏗️"
    mix deps.get

    echo "Rebuilding npm 🌎"
    mix npm.rebuild

    echo "Rebuilding npm 🙄"
    mix npm.install

    echo "Compiling code 🤖"
    mix compile
    say "Compilation finished"
    echo "Compilation finished ✅"
    direnv allow

    # Try to start Phoenix server again
    iex -S mix phx.server
  fi
}

function w () {
  if [[ $PWD/ = */src/cars_platform/apps/* ]]; then
    # If we're in an apps subdirectory, strip the apps/app_name prefix from the test path
    local test_path="${1#apps/*/}"
    echo "Running: mix test.watch $test_path"
    mix test.watch "$test_path"
  else
    cd ~/src/cars_platform
    echo "Running: mix test.watch $1"
    mix test.watch "$1"
  fi
}

function np() {
  export AWS_PROFILE=cars-beta-np-118439772188-developer
  saml2aws login -a cars-beta-np-118439772188-developer
  kubectl exec -it --context np -n cars-web deployment/cars-web-deploy -- bin/cars_web remote
}

function prod() {
  export AWS_PROFILE=cars-platform-256078791775-developer
  saml2aws login -a cars-platform-256078791775-developer
  kubectl exec -it --context prod-242 -n cars-web deployment/cars-web-deploy -- bin/cars_web remote
}

function flaker () {
if [ "$#" -ne 4 ]
then
  NEWLINE=$'\n'
  echo "$(tput setaf 4)$(tput setab 2)\
  Usage: Two arguments must be supplied.\
  ${NEWLINE}The first argument is an integer indicating the number of test runs.\
  ${NEWLINE}The second argument is the path to the test file.\
  ${NEWLINE}The third (optional) argument is the --seed flag.\
  ${NEWLINE}The fourth (optional) argument is the desired seed.\
  ${NEWLINE}Example: $ flaker 10 test/engine/dealers/dealer_test.exs --seed seed
  $(tput sgr0)"
  (exit 33) && true
fi
for (( i=1; i<$1; i++ ));
do
    echo "$(tput setaf 4)$(tput setab 2) $2 test run number: $i$(tput sgr0)";
    mix test $2 $3 $4 || break;
done
}
