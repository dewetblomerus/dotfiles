###### Zshell Configuration
export ZSH=~/.oh-my-zsh
ZSH_THEME="robbyrussell"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
if [ -f $HOME/.ssh/id_rsa ]; then
  plugins=(git ssh-agent)
fi

source $ZSH/oh-my-zsh.sh

# Load all files from .zsh.d directory
if [ -d $HOME/.zsh.d ]; then
  for file in $HOME/.zsh.d/*; do
    source $file
  done
fi

source $HOME/dotfiles/.env

#export NVM_DIR="$HOME/.nvm"
#  . "/usr/local/opt/nvm/nvm.sh"

export GPG_TTY=$(tty)
# export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
# gpgconf --launch gpg-agent

export PATH=/opt/homebrew/bin:$PATH

eval "$(/opt/homebrew/bin/brew shellenv)"

# asdf
. "$HOME/.asdf/asdf.sh"
# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)
# initialise completions with ZSH's compinit
autoload -Uz compinit && compinit

test -f /Users/dewetblomerus/.cars_secrets && source "/Users/dewetblomerus"/.cars_secrets
eval "$(direnv hook zsh)"
